package net.morimekta.file;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NotLinkException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static net.morimekta.file.FileUtil.readCanonicalPath;
import static net.morimekta.file.FileUtil.replaceSymbolicLink;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.jupiter.api.Assertions.fail;

public class FileUtilTest {
    @TempDir
    public File tempDir;
    public Path root;

    @BeforeEach
    public void setUp() throws IOException {
        root = tempDir.getCanonicalFile().getAbsoluteFile().toPath();
    }

    @Test
    public void testReadCanonicalPath() throws IOException {
        Path dot = new File(".").getAbsoluteFile()
                                .getCanonicalFile()
                                .toPath();

        assertThat(readCanonicalPath(Paths.get(".")), is(dot));

        Path target1 = Files.createDirectory(root.resolve("target1"));
        Path target2 = Files.createDirectory(root.resolve("target2"));

        Path link = root.resolve("link");
        replaceSymbolicLink(link, target1);
        assertThat(readCanonicalPath(link), is(target1));
        replaceSymbolicLink(link, target2);

        assertThat(readCanonicalPath(link), is(target2));

        Path link2 = root.resolve("link2");
        replaceSymbolicLink(link2, Paths.get("target1"));
        assertThat(readCanonicalPath(link2), is(target1));

        Path link3 = target1.resolve("link3");
        Path target3 = Paths.get("../target2");
        replaceSymbolicLink(link3, target3);
        assertThat(readCanonicalPath(link3), is(target2));
    }

    @Test
    public void testReadCanonicalPath_fail() {
        try {
            readCanonicalPath(root.resolve("../../../../../../../.."));
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Parent of root does not exist!"));
        }

        try {
            readCanonicalPath(Paths.get("/.."));
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Parent of root does not exist!"));
        }
    }

    @Test
    public void testReplaceSymbolicLink() throws IOException {
        Path dir = Files.createDirectories(root.resolve("dir"));
        Path dir2 = Files.createDirectories(root.resolve("dir2"));

        Path dirLink = root.resolve("link1");
        replaceSymbolicLink(dirLink, dir);
        assertThat(readCanonicalPath(dirLink), is(dir));
        replaceSymbolicLink(dirLink, dir2);
        assertThat(readCanonicalPath(dirLink), is(dir2));

        Path file = Files.createFile(root.resolve("file"));
        Path file2 = Files.createFile(root.resolve("file2"));
        Path fileLink = root.resolve("link2");
        replaceSymbolicLink(fileLink, file);
        assertThat(readCanonicalPath(fileLink), is(file));
        replaceSymbolicLink(fileLink, file2);
        assertThat(readCanonicalPath(fileLink), is(file2));
    }

    @Test
    public void testReplaceSymbolicLinkFail() throws IOException {
        Path file = Files.createFile(root.resolve("file1"));
        Path target = root.resolve("target1");
        try {
            replaceSymbolicLink(file, target);
            fail("no exception");
        } catch (NotLinkException e) {
            assertThat(e.getMessage(), is(root.toString() + "/file1 is not a symbolic link"));
        }
    }

    @Test
    public void testDeleteRecursively() throws IOException {
        Path file = Files.createFile(root.resolve("file1"));
        assertThat(Files.exists(file), is(true));
        FileUtil.deleteRecursively(file);
        assertThat(Files.exists(file), is(false));

        Path dir1 = Files.createDirectories(root.resolve("dir1"));
        Path fileIn1 = Files.createFile(dir1.resolve("file1"));
        Path dir2 = Files.createDirectories(root.resolve("dir2"));
        Path fileIn2 = Files.createFile(dir2.resolve("file2"));
        Path sym = Files.createSymbolicLink(dir1.resolve("dir2"), Path.of("../dir2"));

        Path symResolved = FileUtil.readCanonicalPath(sym);
        assertThat(symResolved, is(dir2));

        assertThat(Files.exists(dir1), is(true));
        assertThat(Files.exists(fileIn1), is(true));
        assertThat(Files.exists(sym), is(true));

        FileUtil.deleteRecursively(dir1);
        assertThat(Files.exists(fileIn1), is(false));
        assertThat(Files.exists(fileIn1), is(false));
        assertThat(Files.exists(dir1), is(false));

        assertThat(Files.exists(fileIn2), is(true));
        assertThat(Files.exists(dir2), is(true));
    }

    @Test
    public void testList() throws IOException {
        Path file = Files.createFile(root.resolve("file"));

        assertThat(FileUtil.list(root), contains(file));
        assertThat(FileUtil.list(file), is(empty()));
    }
}
