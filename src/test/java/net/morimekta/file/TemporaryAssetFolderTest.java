package net.morimekta.file;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class TemporaryAssetFolderTest {
    @TempDir
    public File tempDir;
    public Path root;

    @BeforeEach
    public void setUp() throws IOException {
        root = tempDir.getCanonicalFile().getAbsoluteFile().toPath();
    }

    @Test
    public void testTempAssetFolder() throws IOException {
        Path tempDir;
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder()) {
            tempDir = taf.getPath();
            assertThat(Files.exists(tempDir), is(true));

            assertThat(taf.getFile(), is(taf.getPath().toFile()));
            assertThat(taf.resolvePath("foo").toFile(), is(taf.resolveFile("foo")));

            Files.write(taf.getPath().resolve("foo1"), "bar1".getBytes(UTF_8));
            Files.write(taf.getPath().resolve("foo2"), "bar2".getBytes(UTF_8));
            Files.createSymbolicLink(taf.getPath().resolve("foo3"), Paths.get("foo2"));
            Files.createDirectories(taf.getPath().resolve("foo4"));
            Files.write(taf.getPath().resolve("foo4/foo"), "bar4".getBytes(UTF_8));

            long num = FileUtil.list(taf.getPath()).size();
            assertThat(num, is(4L));
        }

        assertThat(Files.exists(tempDir), is(false));
    }

    @Test
    public void testTempAssetFolder_WithRoot() throws IOException {
        Path tempDir;
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(root)) {
            tempDir = taf.getPath();
            assertThat(taf.getPath().getParent(), is(root));

            assertThat(taf.getFile(), is(taf.getPath().toFile()));
            assertThat(taf.resolvePath("foo").toFile(), is(taf.resolveFile("foo")));

            Files.write(taf.getPath().resolve("foo1"), "bar1".getBytes(UTF_8));
            Files.write(taf.getPath().resolve("foo2"), "bar2".getBytes(UTF_8));
            Files.createSymbolicLink(taf.getPath().resolve("foo3"), Paths.get("foo2"));
            Files.createDirectories(taf.getPath().resolve("foo4"));
            Files.write(taf.getPath().resolve("foo4/foo"), "bar4".getBytes(UTF_8));

            long num = FileUtil.list(root).size();
            assertThat(num, is(1L));

            num = FileUtil.list(taf.getPath()).size();
            assertThat(num, is(4L));
        }

        long num = FileUtil.list(root).size();
        assertThat(num, is(0L));
        assertThat(Files.exists(tempDir), is(false));
    }

    @Test
    public void testTempAssetFolder_File() throws IOException {
        Path tempDir;
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(root)) {
            tempDir = taf.getPath();
            Files.write(taf.getPath().resolve("foo1"), "bar1".getBytes(UTF_8));
            Files.write(taf.getPath().resolve("foo2"), "bar2".getBytes(UTF_8));
            Files.createSymbolicLink(taf.getPath().resolve("foo3"), Paths.get("foo2"));
            Files.createDirectories(taf.getPath().resolve("foo4"));
            Files.write(taf.getPath().resolve("foo4/foo"), "bar4".getBytes(UTF_8));
            Files.write(taf.getPath().resolve("foo4/bar"), "bar5".getBytes(UTF_8));

            long num = FileUtil.list(root).size();
            assertThat(num, is(1L));

            num = FileUtil.list(taf.getPath()).size();
            assertThat(num, is(4L));

            assertThat(taf.list().size(), is(4));
            assertThat(taf.list(true).size(), is(5));
        }

        long num = FileUtil.list(root).size();
        assertThat(num, is(0L));
        assertThat(Files.exists(tempDir), is(false));
    }

    @Test
    public void testTempAssetFolder_Utils() throws IOException {
        Path tempDir;
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(root)) {
            tempDir = taf.getPath();

            taf.put("foo1", "bar1");
            taf.put("foo2/bar", "bar2".getBytes(UTF_8));
            taf.put("foo3", "bar3".getBytes(UTF_8));
            try (OutputStream out = taf.getOutputStream("foo4")) {
                out.write("bar4".getBytes(UTF_8));
            }
            try (OutputStream out = taf.getOutputStream("foo4", true)) {
                out.write(",bar4".getBytes(UTF_8));
            }
            try (Writer writer = taf.getWriter("foo5");
                 Reader reader = taf.getReader("foo4")) {
                writer.append(new BufferedReader(reader).readLine());
                writer.write("\n");
                writer.write("bar5");
            }
            try (Writer writer = taf.getWriter("foo5", true)) {
                writer.write(",bar5");
            }
            try (Writer writer = taf.getWriter("foo6")) {
                writer.write("bar6");
            }

            assertThat(taf.getString("foo1"), is("bar1"));
            assertThat(taf.getBytes("foo2/bar"), is("bar2".getBytes(UTF_8)));
            assertThat(taf.getBytes("foo3"), is("bar3".getBytes(UTF_8)));
            try (InputStream in = taf.getInputStream("foo4")) {
                String all = new String(new BufferedInputStream(in).readAllBytes(), UTF_8);
                assertThat(all, is("bar4,bar4"));
            }
            try (Reader reader = taf.getReader("foo5");
                 BufferedReader br = new BufferedReader(reader)) {
                assertThat(br.readLine(), is("bar4,bar4"));
                assertThat(br.readLine(), is("bar5,bar5"));
            }
            assertThat(taf.getString("foo6"), is("bar6"));
        }

        long num = FileUtil.list(root).size();
        assertThat(num, is(0L));
        assertThat(Files.exists(tempDir), is(false));
    }

    @Test
    public void testTempAssetFolder_BadResolve() throws IOException {
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(root)) {
            try {
                fail("No exception: " + taf.resolvePath(".." + File.separator + "foo"));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Up path not allowed in file name"));
            }
            try {
                fail("No exception: " + taf.resolvePath(String.join(File.separator,
                                                                    List.of("foo", "..", "..", "bar"))));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Up path not allowed in file name"));
            }
            try {
                fail("No exception: " + taf.resolvePath(File.separator + "foo"));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Absolute path not allowed in file name"));
            }
        }
    }
}
