package net.morimekta.file.internal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ListFilesVisitorTest {
    @TempDir
    public File tempDir;
    private Path temp;

    @BeforeEach
    public void setUp() throws IOException {
        temp = tempDir.getAbsoluteFile().getCanonicalFile().toPath();
    }

    @Test
    public void testVisitFileFailed() throws IOException {
        Path file = Files.createFile(temp.resolve("foo"));
        IOException e = new IOException();

        List<Path> list = new ArrayList<>();
        ListFilesVisitor sut = new ListFilesVisitor(temp, true, list::add);
        assertThat(sut.visitFileFailed(file, e), is(FileVisitResult.TERMINATE));
        assertThat(list, is(Collections.emptyList()));
    }

    @Test
    public void testPostVisitDirectory() throws IOException {
        Path file = Files.createFile(temp.resolve("foo"));
        IOException e = new IOException();

        List<Path> list = new ArrayList<>();
        ListFilesVisitor sut = new ListFilesVisitor(temp, true, list::add);

        assertThat(sut.postVisitDirectory(file, null), is(FileVisitResult.CONTINUE));
        assertThat(sut.postVisitDirectory(file, e), is(FileVisitResult.TERMINATE));

        assertThat(list, is(Collections.emptyList()));
    }
}
