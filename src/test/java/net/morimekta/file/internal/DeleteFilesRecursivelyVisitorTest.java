package net.morimekta.file.internal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DeleteFilesRecursivelyVisitorTest {
    AtomicReference<IOException> exception;
    DeleteFilesRecursivelyVisitor sut;

    @TempDir
    public File tempDir;
    private Path temp;

    @BeforeEach
    public void setUp() throws IOException {
        temp = tempDir.getAbsoluteFile().getCanonicalFile().toPath();
        exception = new AtomicReference<>();
        sut = new DeleteFilesRecursivelyVisitor(exception);
    }

    @Test
    public void testPreVisitDirectory() throws IOException {
        Path dir = Files.createDirectories(temp.resolve("tmp"));
        BasicFileAttributes attrs = mock(BasicFileAttributes.class);

        when(attrs.isSymbolicLink()).thenReturn(true);
        assertThat(sut.preVisitDirectory(dir, attrs), is(FileVisitResult.SKIP_SUBTREE));
        assertThat(Files.exists(dir), is(false));

        dir = Files.createDirectories(temp.resolve("tmp"));

        when(attrs.isSymbolicLink()).thenReturn(false);
        assertThat(sut.preVisitDirectory(dir, attrs), is(FileVisitResult.CONTINUE));
        assertThat(Files.exists(dir), is(true));

        assertThat(exception.get(), is(nullValue()));
    }

    @Test
    public void testVisitFileFailed() throws IOException {
        Path file = Files.createFile(temp.resolve("foo"));
        IOException e = new IOException();

        assertThat(sut.visitFileFailed(file, e), is(FileVisitResult.TERMINATE));
        assertThat(exception.get(), is(sameInstance(e)));
    }

    @Test
    public void testPostVisitDirectory() throws IOException {
        Path dir = Files.createDirectories(temp.resolve("tmp"));
        IOException e = new IOException();
        assertThat(sut.postVisitDirectory(dir, e), is(FileVisitResult.TERMINATE));
        assertThat(exception.get(), is(sameInstance(e)));
        assertThat(Files.exists(dir), is(true));

        exception.set(null);

        assertThat(sut.postVisitDirectory(dir, null), is(FileVisitResult.CONTINUE));
        assertThat(exception.get(), is(nullValue()));
        assertThat(Files.exists(dir), is(false));
    }
}
