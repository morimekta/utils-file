package net.morimekta.file;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static net.morimekta.file.FileEvent.CREATED;
import static net.morimekta.file.FileEvent.MODIFIED;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class FileWatcherTest {
    @TempDir
    public  File tempDir;
    private Path tempRoot;

    private Path firstRoot;
    private Path otherRoot;

    private FileWatcher watcher;

    @BeforeEach
    public void setUp() throws IOException {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        tempRoot = tempDir.getAbsoluteFile().getCanonicalFile().toPath();
        firstRoot = Files.createDirectories(tempRoot.resolve("first"));
        otherRoot = Files.createDirectories(tempRoot.resolve("other"));
        watcher = new FileWatcher();
    }

    @AfterEach
    public void tearDown() {
        if (watcher != null) {
            watcher.close();
        }
    }

    @Test
    public void testFileWatcher() throws IOException, InterruptedException {
        FileEventListener listener = mock(FileEventListener.class);
        doThrow(new IllegalArgumentException())
                .when(listener)
                .onFileEvent(any(Path.class), any(FileEvent.class));

        Path file1 = writeAndMove("foo", firstRoot.resolve("foo"));
        Path file2 = writeAndMove("bar", otherRoot.resolve("bar"));

        watcher.addWatcher(file1, listener)
               .addWatcher(file2, listener);

        write("baz", firstRoot.resolve("baz"));
        write("bar", firstRoot.resolve("foo"));

        Thread.sleep(100);

        verify(listener, atLeastOnce()).onFileEvent(firstRoot.resolve("foo"), MODIFIED);
        verifyNoMoreInteractions(listener);
        reset(listener);

        doNothing()
                .when(listener)
                .onFileEvent(any(Path.class), any(FileEvent.class));

        assertThat(watcher.removeWatcher(file1, listener), is(true));
        assertThat(watcher.removeWatcher(file1, listener), is(false));

        write("baz", firstRoot.resolve("foo"));
        write("foo", otherRoot.resolve("bar"));

        Thread.sleep(100);

        verify(listener, atLeastOnce()).onFileEvent(otherRoot.resolve("bar"), MODIFIED);
        verifyNoMoreInteractions(listener);
        reset(listener);

        assertThat(watcher.removeWatcher(listener), is(true));
        assertThat(watcher.removeWatcher(listener), is(false));
    }

    /**
     * Kubernetes configMaps have a rather complicated structure:
     *
     * <pre>{@code
     * cm {
     *     value1 -> ..data/value1
     *     value2 -> ..data/value2
     *     ..data -> ..data-v2
     *     ..data-v1 {
     *         value1
     *         value2
     *     }
     *     ..data-v2 {
     *         value1
     *         value2
     *     }
     * }
     * }</pre>
     *
     * And updates are done by:
     *
     * <ul>
     *     <li>creating the new {@code ..data-vX} directory</li>
     *     <li>remove cm/* value symlinks no longer on {@code ..data-vX}</li>
     *     <li>replace the {@code ..data} symlink to the new directory</li>
     *     <li>add missing valueX symlinks</li>
     *     <li>remove the old {@code ..data-vX} directory</li>
     * </ul>
     *
     * This means if the listened to file is itself a symlink, we must check
     * all the steps in-between, both the symlinks themselves, and if the parent
     * of a file reference is a symlink, that symlink needs to be listened on too.
     */
    @Test
    public void testFileWatcher_k8sConfigMap() throws IOException, InterruptedException {
        Path dataV1 = Files.createDirectories(tempRoot.resolve("..data-v1"));
        Files.writeString(dataV1.resolve("value1"), "V1");
        Files.writeString(dataV1.resolve("value2"), "V1");

        Path data = Files.createSymbolicLink(tempRoot.resolve("..data"), Path.of("..data-v1"));
        Path value1 = Files.createSymbolicLink(tempRoot.resolve("value1"), Path.of("..data/value1"));
        Path value2 = Files.createSymbolicLink(tempRoot.resolve("value2"), Path.of("..data/value2"));

        FileEventListener listener = mock(FileEventListener.class);

        watcher.addWatcher(value1, listener);

        Path dataV2 = Files.createDirectories(tempRoot.resolve("..data-v2"));
        Files.writeString(dataV2.resolve("value1"), "V2");
        Files.writeString(dataV2.resolve("value3"), "V2");

        Files.deleteIfExists(value2);

        FileUtil.replaceSymbolicLink(data, Path.of("..data-v2"));

        Files.createSymbolicLink(tempRoot.resolve("value3"), Path.of("..data/value3"));

        Thread.sleep(250);

        verify(listener).onFileEvent(value1, CREATED);
        verifyNoMoreInteractions(listener);
    }

    @Test
    public void testFileWatcher_weakRefs() throws IOException, InterruptedException {
        FileEventListener listener = mock(FileEventListener.class, "listener");
        // Must be like this to make it GC.
        AtomicReference<FileEventListener> listenerWrapper = new AtomicReference<>(
                (path, event) -> listener.onFileEvent(path, event));

        Path file1 = writeAndMove("foo", firstRoot.resolve("foo"));
        Path file2 = writeAndMove("bar", otherRoot.resolve("bar"));

        assertThat(watcher.weakAddWatcher(file1, listenerWrapper.get()), is(sameInstance(watcher)));
        assertThat(watcher.weakAddWatcher(file2, listenerWrapper.get()), is(sameInstance(watcher)));

        writeAndMove("foo", firstRoot.resolve("foo"));
        writeAndMove("bar", otherRoot.resolve("bar"));

        Thread.sleep(250);

        verify(listener).onFileEvent(file1, CREATED);
        verify(listener).onFileEvent(file2, CREATED);
        verifyNoMoreInteractions(listener);
        reset(listener);

        listenerWrapper.set(null);

        Thread.sleep(250);

        System.gc();

        writeAndMove("foo", firstRoot.resolve("foo"));
        writeAndMove("bar", otherRoot.resolve("bar"));

        verifyNoInteractions(listener);
    }

    @Test
    public void testBadArguments() throws IOException {
        FileEventListener listener = mock(FileEventListener.class, "listener");
        Path file = Files.createFile(tempRoot.resolve("file"));
        Path missing = tempRoot.resolve("missing");

        try {
            watcher.addWatcher(null, listener);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("file == null"));
        }
        try {
            watcher.addWatcher(file, null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("listener == null"));
        }
        try {
            watcher.addWatcher(missing, listener);
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("No such file: " + missing.toString()));
            assertThat(e.getCause(), is(instanceOf(FileNotFoundException.class)));
        }
        try {
            watcher.weakAddWatcher(null, listener);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("file == null"));
        }
        try {
            watcher.weakAddWatcher(file, null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("listener == null"));
        }
        try {
            watcher.weakAddWatcher(missing, listener);
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("No such file: " + missing.toString()));
            assertThat(e.getCause(), is(instanceOf(FileNotFoundException.class)));
        }

        watcher.close();

        try {
            watcher.addWatcher(file, listener);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Starts to watch on closed FileWatcher"));
        }
        try {
            watcher.weakAddWatcher(file, listener);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Starts to watch on closed FileWatcher"));
        }
    }

    static void write(String content, Path file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file.toFile(), false)) {
            fos.write(content.getBytes(UTF_8));
            fos.flush();
            fos.getFD().sync();
        }
    }

    Path writeAndMove(String content, Path file) throws IOException {
        Path tmp = tempRoot.resolve(".ttc-" + ThreadLocalRandom.current().nextLong());
        try (OutputStream fos = Files.newOutputStream(tmp, CREATE_NEW)) {
            fos.write(content.getBytes(UTF_8));
            fos.flush();
        }
        Files.move(tmp, file, REPLACE_EXISTING, ATOMIC_MOVE);
        return file;
    }

}
