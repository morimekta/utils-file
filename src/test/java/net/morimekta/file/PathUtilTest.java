package net.morimekta.file;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import static net.morimekta.file.PathUtil.getFileBaseName;
import static net.morimekta.file.PathUtil.getFileSuffix;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class PathUtilTest {
    @TempDir
    public  File tempDir;
    private Path tempRoot;

    @BeforeEach
    public void setUp() throws IOException {
        tempRoot = tempDir.getAbsoluteFile().getCanonicalFile().toPath();
    }

    @Test
    public void testFileSuffix() {
        assertThat(getFileSuffix(tempRoot), is(""));
        assertThat(getFileSuffix(Path.of("/")), is(""));
        assertThat(getFileSuffix(tempRoot.resolve("foo.bar")), is("bar"));
        assertThat(getFileSuffix(tempRoot.resolve(".bar")), is(""));
        assertThat(getFileSuffix(tempRoot.resolve("..bar")), is(""));
        assertThat(getFileSuffix(tempRoot.resolve("..bar.foo")), is("foo"));
        assertThat(getFileSuffix(tempRoot.resolve("..bar..foo")), is(""));
    }

    @Test
    public void testFileBaseName() {
        assertThat(getFileBaseName(tempRoot), is(not("")));
        assertThat(getFileBaseName(Path.of("/")), is(""));

        assertThat(getFileBaseName(tempRoot.resolve("foo.bar")), is("foo"));
        assertThat(getFileBaseName(tempRoot.resolve(".bar")), is(".bar"));
        assertThat(getFileBaseName(tempRoot.resolve("..bar")), is("..bar"));
        assertThat(getFileBaseName(tempRoot.resolve("..bar.foo")), is("..bar"));
        assertThat(getFileBaseName(tempRoot.resolve("..bar..foo")), is("..bar..foo"));
    }
}
