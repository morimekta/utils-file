package net.morimekta.file;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static net.morimekta.file.FileEvent.CREATED;
import static net.morimekta.file.FileEvent.DELETED;
import static net.morimekta.file.FileEvent.MODIFIED;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class DirWatcherTest {
    @TempDir
    public File tempDir;
    private Path tempRoot;

    private Path firstRoot;
    private Path otherRoot;

    private DirWatcher watcher;

    @BeforeEach
    public void setUp() throws IOException {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        tempRoot = tempDir.getAbsoluteFile().getCanonicalFile().toPath();
        firstRoot = Files.createDirectories(tempRoot.resolve("first"));
        otherRoot = Files.createDirectories(tempRoot.resolve("other"));
        watcher = new DirWatcher();
    }

    @AfterEach
    public void tearDown() {
        if (watcher != null) {
            watcher.close();
        }
    }

    @Test
    public void testDirWatcher() throws IOException, InterruptedException {
        FileEventListener listener = mock(FileEventListener.class, "listener");
        doThrow(new RuntimeException("foo"))
                .when(listener)
                .onFileEvent(any(Path.class), any(FileEvent.class));

        assertThat(watcher.addWatcher(firstRoot, listener), is(sameInstance(watcher)));
        assertThat(watcher.weakAddWatcher(otherRoot, listener), is(sameInstance(watcher)));

        writeAndMoveOver("foo", firstRoot.resolve("foo"));
        writeAndMoveOver("bar", otherRoot.resolve("bar"));

        Thread.sleep(250);

        verify(listener).onFileEvent(firstRoot.resolve("foo"), CREATED);
        verify(listener).onFileEvent(otherRoot.resolve("bar"), CREATED);
        verifyNoMoreInteractions(listener);
        reset(listener);
        doNothing().when(listener)
                   .onFileEvent(any(Path.class), any(FileEvent.class));

        assertThat(watcher.removeWatcher(firstRoot, listener), is(true));
        assertThat(watcher.removeWatcher(firstRoot, listener), is(false));

        write("bar", firstRoot.resolve("bar"));
        write("foo", otherRoot.resolve("foo"));
        Files.deleteIfExists(otherRoot.resolve("foo"));

        Thread.sleep(250);

        verify(listener).onFileEvent(otherRoot.resolve("foo"), CREATED);
        verify(listener).onFileEvent(otherRoot.resolve("foo"), MODIFIED);
        verify(listener).onFileEvent(otherRoot.resolve("foo"), DELETED);
        verifyNoMoreInteractions(listener);
        reset(listener);

        assertThat(watcher.removeWatcher(listener), is(true));
        assertThat(watcher.removeWatcher(listener), is(false));

        writeAndMoveOver("bar", firstRoot.resolve("bar"));
        writeAndMoveOver("foo", otherRoot.resolve("foo"));

        Thread.sleep(250);

        verifyNoInteractions(listener);

        watcher.close();
    }

    @Test
    public void testDirWatcher_weakRefs() throws IOException, InterruptedException {
        FileEventListener listener = mock(FileEventListener.class, "listener");
        // Must be like this to make it GC.
        AtomicReference<FileEventListener> listenerWrapper = new AtomicReference<>(
                (path, event) -> listener.onFileEvent(path, event));

        watcher.weakAddWatcher(firstRoot, listenerWrapper.get());

        writeAndMoveOver("bar", firstRoot.resolve("bar"));

        Thread.sleep(250);

        verify(listener).onFileEvent(firstRoot.resolve("bar"), CREATED);
        verifyNoMoreInteractions(listener);
        reset(listener);

        listenerWrapper.set(null);

        System.gc();

        Thread.sleep(150);

        writeAndMoveOver("foo", firstRoot.resolve("foo"));

        Thread.sleep(250);

        verifyNoInteractions(listener);

        assertThat(watcher.removeWatcher(listener), is(false));
    }

    @Test
    public void testBadArguments() throws IOException {
        FileEventListener listener = mock(FileEventListener.class, "listener");
        Path file = Files.createFile(tempRoot.resolve("file"));
        Path missing = tempRoot.resolve("missing");

        try {
            watcher.addWatcher(null, listener);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("dir == null"));
        }
        try {
            watcher.addWatcher(tempRoot, null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("listener == null"));
        }
        try {
            watcher.addWatcher(file, listener);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a directory: " + file));
        }
        try {
            watcher.weakAddWatcher(null, listener);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("dir == null"));
        }
        try {
            watcher.weakAddWatcher(tempRoot, null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("listener == null"));
        }
        try {
            watcher.weakAddWatcher(file, listener);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a directory: " + file));
        }
        try {
            watcher.addWatcher(missing, listener);
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("No such directory: " + missing.toString()));
            assertThat(e.getCause(), is(instanceOf(FileNotFoundException.class)));
        }

        watcher.close();

        try {
            watcher.addWatcher(tempRoot, listener);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Starts to watch on closed FileWatcher"));
        }
        try {
            watcher.weakAddWatcher(tempRoot, listener);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Starts to watch on closed FileWatcher"));
        }
    }

    @Test
    public void testClose_exceptions() throws IOException, InterruptedException {
        WatchService watchService = mock(WatchService.class);
        ExecutorService watcherExecutor = mock(ExecutorService.class);
        ExecutorService callbackExecutor = mock(ExecutorService.class);
        watcher = new DirWatcher(watchService, watcherExecutor, callbackExecutor);

        verify(watcherExecutor).submit(any(Runnable.class));
        verifyNoMoreInteractions(watchService, watcherExecutor, callbackExecutor);
        reset(watchService, watcherExecutor, callbackExecutor);

        doReturn(false).when(watcherExecutor).isShutdown();
        doThrow(new IOException()).when(watchService).close();
        doThrow(new InterruptedException()).when(watcherExecutor).awaitTermination(10, TimeUnit.SECONDS);
        doThrow(new InterruptedException()).when(callbackExecutor).awaitTermination(10, TimeUnit.SECONDS);

        watcher.close();

        verify(watcherExecutor).isShutdown();
        verify(watcherExecutor).shutdown();
        verify(callbackExecutor).shutdown();
        verify(watchService).close();
        verify(watcherExecutor).awaitTermination(10, TimeUnit.SECONDS);
        verify(callbackExecutor).awaitTermination(10, TimeUnit.SECONDS);
        verifyNoMoreInteractions(watchService, watcherExecutor, callbackExecutor);
    }

    @Test
    public void testClose_timeouts() throws IOException, InterruptedException {
        WatchService watchService = mock(WatchService.class);
        ExecutorService watcherExecutor = mock(ExecutorService.class);
        ExecutorService callbackExecutor = mock(ExecutorService.class);
        watcher = new DirWatcher(watchService, watcherExecutor, callbackExecutor);

        verify(watcherExecutor).submit(any(Runnable.class));
        verifyNoMoreInteractions(watchService, watcherExecutor, callbackExecutor);
        reset(watchService, watcherExecutor, callbackExecutor);

        doReturn(false).when(watcherExecutor).isShutdown();
        doReturn(false).when(watcherExecutor).awaitTermination(10, TimeUnit.SECONDS);
        doReturn(false).when(callbackExecutor).awaitTermination(10, TimeUnit.SECONDS);

        watcher.close();

        verify(watcherExecutor).isShutdown();
        verify(watcherExecutor).shutdown();
        verify(callbackExecutor).shutdown();
        verify(watchService).close();
        verify(watcherExecutor).awaitTermination(10, TimeUnit.SECONDS);
        verify(callbackExecutor).awaitTermination(10, TimeUnit.SECONDS);
        verifyNoMoreInteractions(watchService, watcherExecutor, callbackExecutor);

        reset(watchService, watcherExecutor, callbackExecutor);

        doReturn(true).when(watcherExecutor).isShutdown();

        watcher.close();

        verify(watcherExecutor).isShutdown();
        verifyNoMoreInteractions(watchService, watcherExecutor, callbackExecutor);
    }

    static void write(String content, Path file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file.toFile(), false)) {
            fos.write(content.getBytes(UTF_8));
            fos.flush();
            fos.getFD().sync();
        }
    }

    void writeAndMoveOver(String content, Path file) throws IOException {
        Path tmp = tempRoot.resolve(".ttc-" + ThreadLocalRandom.current().nextLong());
        try (OutputStream fos = Files.newOutputStream(tmp, CREATE_NEW)) {
            fos.write(content.getBytes(UTF_8));
            fos.flush();
        }
        Files.move(tmp, file, REPLACE_EXISTING, ATOMIC_MOVE);
    }
}
