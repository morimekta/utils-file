/*
 * Copyright (c) 2017, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.file;

import java.nio.file.Path;

/**
 * Listener for file events.
 * <p>
 * See {@link DirWatcher} and {@link FileWatcher} for services to handle the
 * actual watching.
 */
@FunctionalInterface
public interface FileEventListener {
    /**
     * Called on each detected file event from {@link DirWatcher} and {@link FileWatcher}.
     *
     * @param file  The file for the event.
     * @param event The event kind. See {@link java.nio.file.StandardWatchEventKinds}.
     */
    void onFileEvent(Path file, FileEvent event);
}
