/**
 * Java module with utilities for handling files, paths and directories.
 */
module net.morimekta.file {
    exports net.morimekta.file;
    requires org.slf4j;
}